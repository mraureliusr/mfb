P=myfirstbeat
CC=gcc
SOX=sox

all:
	$(CC) -lm $(P).c -o $(P)
	./$(P) | head -c 4M > $(P).raw
	$(SOX) -r 12000 -c 2 -t s8 $(P).raw $(P).wav
	oggenc $(P).wav
