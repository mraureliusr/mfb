#include <stdio.h>
#include <math.h>

int main(void) {
    int t = 0, n = 0, q = 0, tt = 0, s1 = 0, s2 = 0;
    for(;;t++) {
        tt = t % 16;
        n = (t >> 11)&t&(t^(1 << t));
        q = (int)(acos(floor(n))*10e3);
        s1 = tt<<1|((t>>2)^((t<<4)&((t<<1)|((t>>12)^(t-4)))))|t>>(11-(1^(4&(t>>15)))&(q<<4|6-(n>>3)))|t>>7;
        s2 = q<<1|((n>>t|1-t)&t>>21)|(t<<1&(t>>9^(t << (1-n))));

        if(t % 2 == 0) {
            putchar(s1);
            putchar(s2);
        } else {
            putchar(s2);
            putchar(s1);
        }
    }
    return 0;
}
