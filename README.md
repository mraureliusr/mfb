# My First Bytebeat

This is my first attempt ever at creating a bytebeat. Upon reading about the concept, I absolutely had to give it a try. So I've been playing around with this very simple source code to generate some weird and wonderful sound. The state of the project currently I am quite happy with. It's turned out more musical than I at first thought I could create.

## How to listen

You need oggenc, sox, and optionally ogg123 to play it in the shell. Simply run the Makefile, and then play the .ogg output. Or, simply grab the pre-encoded ogg from the release page.

## Bytebeat image

![](https://frozendev.tk/~amr/images/mfb.png)